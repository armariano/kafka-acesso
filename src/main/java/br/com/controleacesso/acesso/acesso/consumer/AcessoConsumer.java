package br.com.controleacesso.acesso.acesso.consumer;

import br.com.controleacesso.acesso.acesso.models.dto.AcessoKafka;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;


@Component
public class AcessoConsumer {

    @KafkaListener(topics = "spec3-alder-rienes-1", groupId = "GroupId-Alder")
    public void receber(@Payload AcessoKafka acessoKafka) throws CsvRequiredFieldEmptyException,
            IOException, CsvDataTypeMismatchException {

        gerarArquivoCsv(acessoKafka);

        System.out.println("Cliente Id:" + acessoKafka.getCliente_id()
                + " , Porta: " + acessoKafka.getPorta_id()
                + " , Acesso Valido: " + acessoKafka.isAcessoValido()
                + ", Data de atualizacao: " +  acessoKafka.getDataAtualizacao());
    }

    private static void gerarArquivoCsv(AcessoKafka acessoKafka) throws IOException,
            CsvDataTypeMismatchException, CsvRequiredFieldEmptyException {

           Writer writer = new FileWriter("file-log.csv", true);
           StatefulBeanToCsv<AcessoKafka> beanToCsv =  new StatefulBeanToCsvBuilder(writer).build();
           beanToCsv.write(acessoKafka);
           writer.flush();
           writer.close();
    }
}
